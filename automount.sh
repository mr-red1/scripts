# #!/bin/bash

for label in /dev/disk/by-label/*; do
    # Get the device path from the symlink
    device_path=$(readlink -f "$label")

    # Store the mount points in an array
    mount_points=($(mount | awk '{print $3}'))

    # Check if a specific mount point exists in the array
    check_mount_point="$device_path"
    if [[ " ${mount_points[@]} " =~ " ${check_mount_point} " ]]; then
        echo "Mount point ${check_mount_point} exists."
    else
        echo "Mount point ${check_mount_point} does not exist."

         # Get the label name
        label_name=$(basename "$label")

        # Create the mount point directory based on the label
        mount_point="/mnt/$label_name"
        sudo mkdir -p "$mount_point"

        # Mount the device to the specified mount point
        sudo mount -o rw "$device_path" "$mount_point"

        sudo chmod -R 755 "/mnt/$mount_point"

        echo "Device with label '$label_name' has been mounted to $mount_point."
    fi
done

# to run on boot everytime before network

# crontab -e

# @reboot /path/to/your/script/mount_by_label.sh >/dev/null 2>&1