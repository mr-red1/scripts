# #!/bin/bash
sudo apt-get install git -y

git clone https://gitlab.com/mr-red1/scripts.git

sudo cp ~/scripts/auto-mount.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl enable auto-mount.service

sudo systemctl start auto-mount.service

# for label in /dev/disk/by-label/*; do
#     # Get the device path from the symlink
#     device_path=$(readlink -f "$label")

#     # Get the label name
#     label_name=$(basename "$label")

#     # Create the mount point directory based on the label
#     mount_point="/mnt/$label_name"
#     sudo mkdir -p "$mount_point"

#     # Mount the device to the specified mount point
#     sudo mount "$device_path" "$mount_point"

#     echo "Device with label '$label_name' has been mounted to $mount_point."
# done

# to run on boot everytime before network

# crontab -e

# @reboot /path/to/your/script/mount_by_label.sh >/dev/null 2>&1